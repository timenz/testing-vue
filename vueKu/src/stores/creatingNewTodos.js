import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: [
      {
        task: 'Code',
        completed: true
      },
      {
        task: 'Sleep',
        completed: false
      },
      {
        task: 'Eat',
        completed: false
      }
    ]
  },
  getters: {
    todos: state => state.todos
  },
  mutations: {
    // karena adding todo list akan mengubah state.todos, maka masuk ke mutations
    addTodo: (state, taskName) => {
      // buat object baru
      const task = {
        task: taskName,
        completed: false
      }
      // push ke todos
      state.todos.push(task)
    }
  }
})

import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import myVuex from '@/components/myVuex'
import listingTodos from '@/components/listingTodos'
import creatingNewTodos from '@/components/creatingNewTodos'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/myVuex',
      name: 'myVuex',
      component: myVuex
    },
    {
      path: '/listingTodos',
      name: 'listingTodos',
      component: listingTodos
    },
    {
      path: '/creatingNewTodos',
      name: 'creatingNewTodos',
      component: creatingNewTodos
    }
  ]
})
